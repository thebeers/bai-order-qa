// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// request promise to send credit card validatoin to merchant warrior
const rp = require('request-promise');
const convert = require('xml-js');

// JSON body parser for HTTP requests
var bodyParser = require('body-parser');

// get twilio for messaging to non message enabled devices
const twilio = require('twilio');
//config twilio sid
const twilioSid=functions.config().twilio.sid;
//config twilio auth token
const twilioAuthToken=functions.config().twilio.token;
// get a twilio client
const client=new twilio(twilioSid,twilioAuthToken);

const stripe =require('stripe');

// let Pin = require('pinjs');
//const thisProj = {allowOrigin: 'https://yarrababs.web.app', mwURL: "https://api.merchantwarrior.com/post/", apiKey: "bpaf0ceo", apiPassPhrase: "d6edg3gz"}; // PROD
//const thisProj = {allowOrigin: 'https://bai-order-qa.web.app', mwURL: "https://base.merchantwarrior.com/post/", apiKey: "feco0apb", apiPassPhrase: "dg6zdge3"}; // QA
// The Firebase Admin SDK to access the Firebase Realtime Database.
var crypto = require('crypto');
const admin = require('firebase-admin');
admin.initializeApp();
let db = admin.firestore();
var thisProj;

// Realtime Database under the path /messages/:pushId/original
exports.makePayment = functions.https.onRequest((request, response) => {

  let priceMapping = {};
  let banquetPriceMapping = {};
  let banquetPeopleMapping = {};
  let menuData = [];
  let paymentDetails = request.body;  
  let orderDetails = JSON.parse(Buffer.from(paymentDetails.encodedOrder, 'base64').toString());
  let orderName = Buffer.from(paymentDetails.orderName, 'base64').toString();  
  let orderTime = Buffer.from(paymentDetails.orderTime, 'base64').toString();
  let messageToken = paymentDetails.messageTokenHidden;
  let contactNumber = Buffer.from(paymentDetails.contactNumber, 'base64').toString();
  let address = {};
  if (paymentDetails.hasOwnProperty('address')) {
    address = JSON.parse(Buffer.from(paymentDetails.address, 'base64').toString());
  }
  let mobileNumber = '';
  if (paymentDetails.hasOwnProperty('mobileNumber')) {
    mobileNumber = Buffer.from(paymentDetails.mobileNumber, 'base64').toString();  
  }
  let transactionAmount = 0;
  let orderIds = [];

  db.collection('project').get().then(function(snapshot) {
    snapshot.forEach(function (item) {
      thisProj = item.data();
    });
  }).then(function () {

    response.header('Access-Control-Allow-Origin', thisProj.allowOrigin);
        
    if (request.get('origin') != thisProj.allowOrigin) {
      return;
    }
      

    if (Array.isArray(orderDetails)) {
      orderDetails.forEach(function (items) {
        for (let property in items) {
          if (items.hasOwnProperty(property) && Array.isArray(items[property])) {
            items[property].forEach(function (item) {
              orderIds.push(item.id);
              if (item.hasOwnProperty('qty')) {
                banquetPeopleMapping[item.id] = item.qty;
              }
            })
          }
        }
      })
    }

    db.collection('menu').get().then(function (snapshot) {
      snapshot.forEach(function (menu) {
         menuData.push(menu.data());
      });
      fuckMe(menuData);
      if (Object.keys(address).length > 0) {
        var delivery = {};
        db.collection('vendor').doc('delivery').get().then(function(doc) {
          delivery = doc.data()
        }).then(function () {
          console.log('Got delivery');    
          tempTotal = 0;
          getTotalPrice(tempTotal);
          //if (delivery.discountSuburbs.includes(address['suburb'])) {
          if (tempTotal >= delivery.minOrderPrice) {
            transactionAmount = delivery.discountPrice;
          } else {
            transactionAmount = delivery.fullPrice;
          }
          doIt();
        });
      } else {
       doIt();
      }
    });
  }); 
  
  function doIt() {
  
      transactionFormattedAmount = getTotalPrice(transactionAmount);
      console.log(transactionFormattedAmount);
      const transactionCurrency = "AUD";
      const customerCountry = "AU"
      const customerState = "ACT"
      const customerCity = "Canberra";
      const customerAddress = "yarra";
      let customerPostCode = "2600";
      /** variables taken from the request **/
      let paymentCardNumber = paymentDetails.number.replace(/\s/g, '');
      let paymentCardExpiry = paymentDetails.expiry.replace(/\s/g, '').replace('/', '');
      let customerName = paymentDetails.name;
      let creditCardSecurityCode = paymentDetails.cvc;
  
      var result = getResult(thisProj.apiPassPhrase);
      var concatenated = new String(result + thisProj.merchantUUID + transactionFormattedAmount + transactionCurrency).toLowerCase();
      var finalHash = getResult(concatenated);
      var options = {
        method: 'POST',
        uri: thisProj.mwURL,
        form: {
          "method": "processCard",
          "merchantUUID": thisProj.merchantUUID,
          "apiKey": thisProj.apiKey,
          "transactionAmount": transactionFormattedAmount,
          "transactionCurrency": transactionCurrency,
          "transactionProduct": "software",
          "customerName": "yarrBabs",
          "customerCountry": customerCountry,
          "customerState": customerState,
          "customerCity": customerCity,
          "customerAddress": customerAddress,
          "customerPostCode": customerPostCode,
          "paymentCardNumber": paymentCardNumber,
          "paymentCardExpiry": paymentCardExpiry,
          "paymentCardName": customerName,
          "paymentCardCSC": creditCardSecurityCode,
          "hash": finalHash
        },
        headers: {
          /* 'content-type': 'application/x-www-form-urlencoded' */ // Is set automatically
        }
      };
      rp(options).then(function (body) {
            
        let xml = body
        let MWResponseXML = JSON.parse(convert.xml2json(xml, {compact: true, spaces: 4}));
        let orderNumDocRef = db.collection("orderNum").doc("sequentialNum");
        let orderNum;
        if (MWResponseXML.mwResponse.responseCode._text === '0') {
          db.runTransaction(function (transaction) {
            return transaction.get(orderNumDocRef).then(function (orderNumDoc) {
              if (!orderNumDoc.exists) {
                orderNumDocRef.set({number: 0});
              }
              orderNum = orderNumDoc.data().number + 1;
              if (orderNum > 999) {
                orderNum = 1;
              }
              transaction.update(orderNumDocRef, {number: orderNum});
            }).then(function () {
              var now = new Date();
              var status = 'pending';
              if (orderTime == 'asap') {
                status = 'inProgress';
              }
              orderObj = {
                name: orderName,
                token: messageToken,
                status: status,
                orderTime: orderTime,
                order: orderDetails,
                orderNum: orderNum,
                contactNumber: contactNumber,
                address: address,
                submitTime: now.getTime()
              };
              if (mobileNumber !== '') {
                orderObj['mobileNumber'] = mobileNumber;
              }
              db.collection("pendingOrders").doc().set(orderObj).then(function () {
                orderDetails = [];
                orderName = '';
                let orderDetailsResponse = {'orderName': orderName, 'orderNum': orderNum,};
                return response.send(orderDetailsResponse);
              }).catch(function (error) {
                console.error("Error writing document: ", error);
              });
            });
          });
        } else {
          return response.send({'declined':true});
        }
      }).catch(function (err) {
        console.log('error in makepayment ' + err);
        return response.send(err);
      });
  }


    function fuckMe(menuData) {
        menuData.forEach(function (menuItem) {
            for (let property in menuItem.extras) {

                if (menuItem.extras.hasOwnProperty(property) && Array.isArray(menuItem.extras[property])) {
                    menuItem.extras[property].forEach(function (item) {
                        priceMapping[item.id] = item.price;
                    })
                }
            }
            if (menuItem.hasOwnProperty('perPersonPrice')) {
                menuItem.types.forEach(function (item) {
                    banquetPriceMapping[item.id] = {
                        'extraPrice': item.extraPrice,
                        'price': item.price,
                        'minPeople': menuItem.minPeople
                    };
                });
            } else {

                menuItem.types.forEach(function (item) {
                    priceMapping[item.id] = item.price;

                });
            }
        });
    }


    function getTotalPrice(t) {
        orderIds.forEach(function (key) {
            if (priceMapping[key] != null) {
                t = t + priceMapping[key];
            }
            if (banquetPriceMapping[key] != null) {
                t = t + banquetPriceMapping[key].price;
                t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] ));
                // t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] - parseInt(banquetPriceMapping[key].minPeople)));
            }


        })
        return Number(Math.round(t + 'e' + 2) + 'e-' + 2).toFixed(2);
    }

    /***payment details is gonna be an array(check this here) if so check how to get object properties***/


    function getResult(param) {
        return crypto.createHash('md5').update(param).digest("hex");
    }


})


exports.stripeConnectEndpoint = functions.https.onRequest((request, response) => {

    response.send('Stripe Endpoint');
})


exports.transactionWebhook = functions.https.onRequest((request, response) => {

    // response.send('stripe account endpoint');
    
    const sig = request.headers['stripe-signature'];

    let event;
    // need to pull from firestore to quickly change between test / prod
    let endpointSecret = 'whsec_dvuBVy61GUqnwZl1vNukbG9n2HNwQHFI';

    try {
      event = stripe.webhooks.constructEvent(request.rawBody, sig, endpointSecret);
      console.log(event);
    }
    catch (err) {
      console.log(err);
      response.status(400).send(`Webhook Error: ${err.message}`);
    }

    // Handle the event
    switch (event.type) {
      // Process the order
      case 'charge.succeeded':
        const paymentIntent = event.data.object;
        console.log('PaymentIntent was successful!');
        
        // We need to find out if the event has the secret??? I need something to use to find the order in the temp spaces
        // man i hope the secret is unique
        try {
          
          admin.firestore().collection('intentOrders').where("clientSecret", "==", paymentIntent.payment_intent).get().then(function (querySnapshot) {
            // There should be only 1
            console.log('Moving order');
            moveMe = querySnapshot.docs[0].data();
            admin.firestore().collection('pendingOrders').add(moveMe).then(function(docRef) {
                console.log("Document written with ID: ", docRef.id);
            })
            .catch(function(error) {
                console.error("Error adding document: ", error);
            });
            
            //Delete anything with this secret from the intent
            querySnapshot.forEach(function (doc) {
              admin.firestore().collection('intentOrders').doc(doc.id).delete();
            });
          });
        } catch(e){
          console.log(e);
        }
        
        break;
      // Future things here
      case 'payment_method.attached':
        const paymentMethod = event.data.object;
        console.log('PaymentMethod was attached to a Customer!')
        break;
      // ... handle other event types
      default:
        // Unexpected event type
          // db.collection('intentOrders').where("clientSecret", "==", event.data.object.client_secret).get().then(function (querySnapshot) {
          // // There should be only 1
          // moveMe = querySnapshot.data();
          //     // moveMe = querySnapshot.docs()[0].data();
          // // db.collection('pendingOrders').add(moveMe);
          // console.log('mofo');
          // console.log(moveMe);
          //
          // // Delete anything with this secret from the intent
          // // querySnapshot.forEach(function (doc) {
          // //     db.collection('intentOrders').doc(doc.id).delete();
          // });
          // console.log(event);
        return response.status(400).end();
    }

    // Return a response to acknowledge receipt of the event
    return response.status(200).end();
    //response.json({received: true});
})


exports.makePaymentWithStripe = functions.https.onRequest((request, response) => {

    let priceMapping = {};
    let banquetPriceMapping = {};
    let banquetPeopleMapping = {};
    let menuData = [];
    let paymentDetails = request.body;
    let orderDetails = JSON.parse(Buffer.from(paymentDetails.encodedOrder, 'base64').toString());
    let orderName = Buffer.from(paymentDetails.orderName, 'base64').toString();
    let orderTime = Buffer.from(paymentDetails.orderTime, 'base64').toString();
    let messageToken = paymentDetails.messageTokenHidden;
    let contactNumber = Buffer.from(paymentDetails.contactNumber, 'base64').toString();
    let address = {};
    if (paymentDetails.hasOwnProperty('address')) {
        address = JSON.parse(Buffer.from(paymentDetails.address, 'base64').toString());
    }
    let mobileNumber = '';
    if (paymentDetails.hasOwnProperty('mobileNumber')) {
        mobileNumber = Buffer.from(paymentDetails.mobileNumber, 'base64').toString();
    }
    let transactionAmount = 0;
    let orderIds = [];

    db.collection('project').get().then(function(snapshot) {
        snapshot.forEach(function (item) {
            thisProj = item.data();
        });
    }).then(function () {

        response.header('Access-Control-Allow-Origin', thisProj.allowOrigin);

        if (request.get('origin') != thisProj.allowOrigin) {
            return;
        }


        if (Array.isArray(orderDetails)) {
            orderDetails.forEach(function (items) {
                for (let property in items) {
                    if (items.hasOwnProperty(property) && Array.isArray(items[property])) {
                        items[property].forEach(function (item) {
                            orderIds.push(item.id);
                            if (item.hasOwnProperty('qty')) {
                                banquetPeopleMapping[item.id] = item.qty;
                            }
                        })
                    }
                }
            })
        }

        db.collection('menu').get().then(function (snapshot) {
            snapshot.forEach(function (menu) {
                menuData.push(menu.data());
            });
            fuckMe(menuData);
            if (Object.keys(address).length > 0) {
                var delivery = {};
                db.collection('vendor').doc('delivery').get().then(function(doc) {
                    delivery = doc.data()
                }).then(function () {
                    console.log('Got delivery');
                    tempTotal = 0;
                    getTotalPrice(tempTotal);
                    //if (delivery.discountSuburbs.includes(address['suburb'])) {
                    if (tempTotal >= delivery.minOrderPrice) {
                        transactionAmount = delivery.discountPrice;
                    } else {
                        transactionAmount = delivery.fullPrice;
                    }
                    doIt();
                });
            } else {
                doIt();
            }
        });
    });

    function doIt() {

        transactionFormattedAmount = getTotalPrice(transactionAmount);
        console.log(transactionFormattedAmount);
        const transactionCurrency = "AUD";
        const customerCountry = "AU"
        const customerState = "ACT"
        const customerCity = "Canberra";
        const customerAddress = "yarra";
        let customerPostCode = "2600";
        /** variables taken from the request **/
        let paymentCardNumber = paymentDetails.number.replace(/\s/g, '');
        let paymentCardExpiry = paymentDetails.expiry.replace(/\s/g, '').replace('/', '');
        let customerName = paymentDetails.name;
        let creditCardSecurityCode = paymentDetails.cvc;

        var result = getResult(thisProj.apiPassPhrase);
        var concatenated = new String(result + thisProj.merchantUUID + transactionFormattedAmount + transactionCurrency).toLowerCase();
        var finalHash = getResult(concatenated);
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        const stripe = require('stripe')('sk_test_mbXNT52qBpnuOaEQyFIqTt9h00ZR2Ymk9K');

        stripe.charges.create({
            amount: 1000,
            currency: "aud",
            source: "tok_visa",
            transfer_data: {
                amount: 877,
                destination: "{{CONNECTED_STRIPE_ACCOUNT_ID}}",
            },
        }).then(function(charge) {
            // asynchronously called
        });
        rp(options).then(function (body) {

            let xml = body
            let MWResponseXML = JSON.parse(convert.xml2json(xml, {compact: true, spaces: 4}));
            let orderNumDocRef = db.collection("orderNum").doc("sequentialNum");
            let orderNum;
            if (MWResponseXML.mwResponse.responseCode._text === '0') {
                db.runTransaction(function (transaction) {
                    return transaction.get(orderNumDocRef).then(function (orderNumDoc) {
                        if (!orderNumDoc.exists) {
                            orderNumDocRef.set({number: 0});
                        }
                        orderNum = orderNumDoc.data().number + 1;
                        if (orderNum > 999) {
                            orderNum = 1;
                        }
                        transaction.update(orderNumDocRef, {number: orderNum});
                    }).then(function () {
                        var now = new Date();
                        var status = 'pending';
                        if (orderTime == 'asap') {
                            status = 'inProgress';
                        }
                        orderObj = {
                            name: orderName,
                            token: messageToken,
                            status: status,
                            orderTime: orderTime,
                            order: orderDetails,
                            orderNum: orderNum,
                            contactNumber: contactNumber,
                            address: address,
                            submitTime: now.getTime()
                        };
                        if (mobileNumber !== '') {
                            orderObj['mobileNumber'] = mobileNumber;
                        }
                        db.collection("pendingOrders").doc().set(orderObj).then(function () {
                            orderDetails = [];
                            orderName = '';
                            let orderDetailsResponse = {'orderName': orderName, 'orderNum': orderNum,};
                            return response.send(orderDetailsResponse);
                        }).catch(function (error) {
                            console.error("Error writing document: ", error);
                        });
                    });
                });
            } else {
                return response.send({'declined':true});
            }
        }).catch(function (err) {
            console.log('error in makepayment ' + err);
            return response.send(err);
        });
    }


    function fuckMe(menuData) {
        menuData.forEach(function (menuItem) {
            for (let property in menuItem.extras) {

                if (menuItem.extras.hasOwnProperty(property) && Array.isArray(menuItem.extras[property])) {
                    menuItem.extras[property].forEach(function (item) {
                        priceMapping[item.id] = item.price;
                    })
                }
            }
            if (menuItem.hasOwnProperty('perPersonPrice')) {
                menuItem.types.forEach(function (item) {
                    banquetPriceMapping[item.id] = {
                        'extraPrice': item.extraPrice,
                        'price': item.price,
                        'minPeople': menuItem.minPeople
                    };
                });
            } else {

                menuItem.types.forEach(function (item) {
                    priceMapping[item.id] = item.price;

                });
            }
        });
    }


    function getTotalPrice(t) {
        orderIds.forEach(function (key) {
            if (priceMapping[key] != null) {
                t = t + priceMapping[key];
            }
            if (banquetPriceMapping[key] != null) {
                t = t + banquetPriceMapping[key].price;
                t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] ));
                // t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] - parseInt(banquetPriceMapping[key].minPeople)));
            }


        })
        return Number(Math.round(t + 'e' + 2) + 'e-' + 2).toFixed(2);
    }

    /***payment details is gonna be an array(check this here) if so check how to get object properties***/


    function getResult(param) {
        return crypto.createHash('md5').update(param).digest("hex");
    }


})

/** This is different as were only calculating the price to then get a 'client_secret', the card processing relies on
 * this but we're not writing the order to database yet ....or do we with payment var set to false till later ...grrr
 * */
exports.stripeClientSecret = functions.https.onRequest((request, response) => {



    let priceMapping = {};
    let banquetPriceMapping = {};
    let banquetPeopleMapping = {};
    let menuData = [];
    let paymentDetails = request.body;
    console.log(paymentDetails);
    let orderDetails = JSON.parse(Buffer.from(paymentDetails.encodedOrder, 'base64').toString());
    let orderName = Buffer.from(paymentDetails.orderName, 'base64').toString();
    let orderTime = Buffer.from(paymentDetails.orderTime, 'base64').toString();
    let messageToken = '';
    if (paymentDetails.hasOwnProperty('messageTokenHidden')) {
      messageToken = paymentDetails.messageTokenHidden;
    }
    let contactNumber = Buffer.from(paymentDetails.contactNumber, 'base64').toString();
    let address = {};
    if (paymentDetails.hasOwnProperty('address')) {
        address = JSON.parse(Buffer.from(paymentDetails.address, 'base64').toString());
    }
    let mobileNumber = '';
    if (paymentDetails.hasOwnProperty('mobileNumber')) {
        mobileNumber = Buffer.from(paymentDetails.mobileNumber, 'base64').toString();
    }
    let transactionAmount = 0;
    let orderIds = [];

    db.collection('project').get().then(function(snapshot) {
        snapshot.forEach(function (item) {
            thisProj = item.data();
        });
    }).then(function () {

        response.header('Access-Control-Allow-Origin', thisProj.allowOrigin);

        if (request.get('origin') != thisProj.allowOrigin) {
            return;
        }


        if (Array.isArray(orderDetails)) {
            orderDetails.forEach(function (items) {
                for (let property in items) {
                    if (items.hasOwnProperty(property) && Array.isArray(items[property])) {
                        items[property].forEach(function (item) {
                            orderIds.push(item.id);
                            if (item.hasOwnProperty('qty')) {
                                banquetPeopleMapping[item.id] = item.qty;
                            }
                        })
                    }
                }
            })
        }

        db.collection('menu').get().then(function (snapshot) {
            snapshot.forEach(function (menu) {
                menuData.push(menu.data());
            });
            fuckMe(menuData);
            if (Object.keys(address).length > 0) {
                var delivery = {};
                db.collection('vendor').doc('delivery').get().then(function(doc) {
                    delivery = doc.data()
                }).then(function () {
                    console.log('Got delivery');
                    tempTotal = 0;
                    getTotalPrice(tempTotal);
                    //if (delivery.discountSuburbs.includes(address['suburb'])) {
                    if (tempTotal >= delivery.minOrderPrice) {
                        transactionAmount = delivery.discountPrice;
                    } else {
                        transactionAmount = delivery.fullPrice;
                    }
                    doIt();
                });
            } else {
                doIt();
            }
        });
    });

    function doIt  () {

        transactionFormattedAmount = getTotalPrice(transactionAmount);
       
        const stripe = require('stripe')('sk_test_mbXNT52qBpnuOaEQyFIqTt9h00ZR2Ymk9K');
        let intent = null;
        try {
            
          intent = stripe.paymentIntents.create({
              amount: transactionFormattedAmount,
              currency: 'aud',
              payment_method_types: ['card'],
          },function(err, paymentIntent){
           
            // Right here, we need to push the order to the db just like the MW doIt()
            docRef = '';
            let orderNumDocRef = db.collection("orderNum").doc("sequentialNum");
            let orderNum;
            db.runTransaction(function (transaction) {
              return transaction.get(orderNumDocRef).then(function (orderNumDoc) {
                if (!orderNumDoc.exists) {
                    orderNumDocRef.set({number: 0});
                }
                orderNum = orderNumDoc.data().number + 1;
                if (orderNum > 999) {
                    orderNum = 1;
                }
                transaction.update(orderNumDocRef, {number: orderNum});
              }).then(function () {
                var now = new Date();
                var status = 'pending';
                if (orderTime == 'asap') {
                    status = 'inProgress';
                }
                orderObj = {
                  name: orderName,
                  token: messageToken,
                  status: status,
                  orderTime: orderTime,
                  order: orderDetails,
                  orderNum: orderNum,
                  clientSecret: paymentIntent.id,
                    // clientSecret: paymentIntent.client_secret,
                  contactNumber: contactNumber,
                  address: address,
                  submitTime: now.getTime()
                };
                if (mobileNumber !== '') {
                  orderObj['mobileNumber'] = mobileNumber;
                }
                console.log(orderObj);
                db.collection("intentOrders").add(orderObj).then(function(ref) {
                  orderDetails = [];
                  orderName = '';
                  docRef = ref.id;
                  //let orderDetailsResponse = {'orderName': orderName, 'orderNum': orderNum,};
                  //return response.send(orderDetailsResponse);
                  return response.send({cs: paymentIntent.client_secret, orderNum: orderObj.orderNum});
                }).catch(function (error) {
                  console.error("Error writing document: ", error);
                });
              });
            });
            
            
          });
             
        } catch (e) {
            console.log(e.toString());
            return response.send({'declined':true});
        }


     
    }

    function fuckMe(menuData) {
        menuData.forEach(function (menuItem) {
            for (let property in menuItem.extras) {

                if (menuItem.extras.hasOwnProperty(property) && Array.isArray(menuItem.extras[property])) {
                    menuItem.extras[property].forEach(function (item) {
                        priceMapping[item.id] = item.price;
                    })
                }
            }
            if (menuItem.hasOwnProperty('perPersonPrice')) {
                menuItem.types.forEach(function (item) {
                    banquetPriceMapping[item.id] = {
                        'extraPrice': item.extraPrice,
                        'price': item.price,
                        'minPeople': menuItem.minPeople
                    };
                });
            } else {

                menuItem.types.forEach(function (item) {
                    priceMapping[item.id] = item.price;

                });
            }
        });
    }
//
//
    function getTotalPrice(t) {
        orderIds.forEach(function (key) {
            if (priceMapping[key] != null) {
                t = t + priceMapping[key];
            }
            if (banquetPriceMapping[key] != null) {
                t = t + banquetPriceMapping[key].price;
                t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] ));
                // t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] - parseInt(banquetPriceMapping[key].minPeople)));
            }


        })
        // Pin is in cents
        t = t*100;
        return Number(Math.round(t + 'e' + 2) + 'e-' + 2);
    }

});


exports.testClientSecret  = functions.https.onRequest((request,response) => {

    //TODO: make the key like apiKey from firestore
    const stripe = require('stripe')('sk_test_mbXNT52qBpnuOaEQyFIqTt9h00ZR2Ymk9K');

    (async () => {
        const intent = await stripe.paymentIntents.create({
            amount: 1000,
            currency: 'aud',
        });

        response.send(intent);
    })();
});

// exports.makePaymentWithPIN = functions.https.onRequest((request, response) => {
//
//     let priceMapping = {};
//     let banquetPriceMapping = {};
//     let banquetPeopleMapping = {};
//     let menuData = [];
//     let paymentDetails = request.body;
//     let orderDetails = JSON.parse(Buffer.from(paymentDetails.encodedOrder, 'base64').toString());
//     let orderName = Buffer.from(paymentDetails.orderName, 'base64').toString();
//     let orderTime = Buffer.from(paymentDetails.orderTime, 'base64').toString();
//     let messageToken = paymentDetails.messageTokenHidden;
//     let contactNumber = Buffer.from(paymentDetails.contactNumber, 'base64').toString();
//     let address = {};
//     if (paymentDetails.hasOwnProperty('address')) {
//         address = JSON.parse(Buffer.from(paymentDetails.address, 'base64').toString());
//     }
//     let mobileNumber = '';
//     if (paymentDetails.hasOwnProperty('mobileNumber')) {
//         mobileNumber = Buffer.from(paymentDetails.mobileNumber, 'base64').toString();
//     }
//     let transactionAmount = 0;
//     let orderIds = [];
//
//     db.collection('project').get().then(function(snapshot) {
//         snapshot.forEach(function (item) {
//             thisProj = item.data();
//         });
//     }).then(function () {
//
//         response.header('Access-Control-Allow-Origin', thisProj.allowOrigin);
//
//         if (request.get('origin') != thisProj.allowOrigin) {
//             return;
//         }
//
//
//         if (Array.isArray(orderDetails)) {
//             orderDetails.forEach(function (items) {
//                 for (let property in items) {
//                     if (items.hasOwnProperty(property) && Array.isArray(items[property])) {
//                         items[property].forEach(function (item) {
//                             orderIds.push(item.id);
//                             if (item.hasOwnProperty('qty')) {
//                                 banquetPeopleMapping[item.id] = item.qty;
//                             }
//                         })
//                     }
//                 }
//             })
//         }
//
//         db.collection('menu').get().then(function (snapshot) {
//             snapshot.forEach(function (menu) {
//                 menuData.push(menu.data());
//             });
//             fuckMe(menuData);
//             if (Object.keys(address).length > 0) {
//                 var delivery = {};
//                 db.collection('vendor').doc('delivery').get().then(function(doc) {
//                     delivery = doc.data()
//                 }).then(function () {
//                     console.log('Got delivery');
//                     tempTotal = 0;
//                     getTotalPrice(tempTotal);
//                     //if (delivery.discountSuburbs.includes(address['suburb'])) {
//                     if (tempTotal >= delivery.minOrderPrice) {
//                         transactionAmount = delivery.discountPrice;
//                     } else {
//                         transactionAmount = delivery.fullPrice;
//                     }
//                     doIt();
//                 });
//             } else {
//                 doIt();
//             }
//         });
//     });
//
//     function doIt() {
//
//         transactionFormattedAmount = getTotalPrice(transactionAmount);
//         console.log(transactionFormattedAmount);
//         const transactionCurrency = "AUD";
//         const customerCountry = "AU"
//         const customerState = "ACT"
//         const customerCity = "Canberra";
//         const customerAddress = "yarra";
//         let customerPostCode = "2600";
//         /** variables taken from the request **/
//         let paymentCardNumber = paymentDetails.number.replace(/\s/g, '');
//         let paymentCardExpiryMonth = paymentDetails.expiry.replace(/\s/g, '').split('/')[0];
//         let paymentCardExpiryYear = paymentDetails.expiry.replace(/\s/g, '').split('/')[1];
//         let customerName = paymentDetails.name;
//         let creditCardSecurityCode = paymentDetails.cvc;
//
//         var result = getResult(thisProj.apiPassPhrase);
//         var concatenated = new String(result + thisProj.merchantUUID + transactionFormattedAmount + transactionCurrency).toLowerCase();
//         var finalHash = getResult(concatenated);
//         var pin = Pin.setup({
//             key: thisProj.pinKey,
//             production: false
//         });
//
//
//
//
//         /*var options = {
//             method: 'POST',
//             uri: thisProj.mwURL,
//             form: {
//                 "method": "processCard",
//                 "merchantUUID": thisProj.merchantUUID,
//                 "apiKey": thisProj.apiKey,
//                 "transactionAmount": transactionFormattedAmount,
//                 "transactionCurrency": transactionCurrency,
//                 "transactionProduct": "software",
//                 "customerName": "yarrBabs",
//                 "customerCountry": customerCountry,
//                 "customerState": customerState,
//                 "customerCity": customerCity,
//                 "customerAddress": customerAddress,
//                 "customerPostCode": customerPostCode,
//                 "paymentCardNumber": paymentCardNumber,
//                 "paymentCardExpiry": paymentCardExpiry,
//                 "paymentCardName": customerName,
//                 "paymentCardCSC": creditCardSecurityCode,
//                 "hash": finalHash
//             },
//             headers: {
//                 /* 'content-type': 'application/x-www-form-urlencoded'  // Is set automatically
//             }
//         };*/
//
//
//         //TODO: format stuff getting sent in ie expiry is being broken into yea and month
//         pin.createCharge({
//                 amount: transactionFormattedAmount,
//                 description: 'belco babs order',
//                 email: 'roland@pinpayments.com',
//                 ip_address: '203.192.1.172',
//                 card: {
//                     number: paymentCardNumber,
//                     expiry_month: paymentCardExpiryMonth,
//                     expiry_year: paymentCardExpiryYear,
//                     cvc: creditCardSecurityCode,
//                     name: customerName,
//                     address_line1: 'Lathlain St',
//                     address_city: 'Belconnen',
//                     address_postcode: 2600,
//                     address_state: 'ACT',
//                     address_country: 'AU'
//                 }
//             },
//             /**
//              * @param error - Error response from the API
//              * @param response - A HTTP response object.
//              * @param body - The JSON response body.
//              */
//             function (error, myResponse, body) {
//                 console.log(body.response.success);
//                 let orderNumDocRef = db.collection("orderNum").doc("sequentialNum");
//                 let orderNum;
//                 if (body.response.success === true) {
//                   db.runTransaction(function (transaction) {
//                      return transaction.get(orderNumDocRef).then(function (orderNumDoc) {
//                          if (!orderNumDoc.exists) {
//                              orderNumDocRef.set({number: 0});
//                          }
//                          orderNum = orderNumDoc.data().number + 1;
//                          if (orderNum > 999) {
//                              orderNum = 1;
//                          }
//                          transaction.update(orderNumDocRef, {number: orderNum});
//                      }).then(function () {
//                          var now = new Date();
//                          var status = 'pending';
//                          if (orderTime == 'asap') {
//                             status = 'inProgress';
//                          }
//                          orderObj = {
//                              name: orderName,
//                              token: messageToken,
//                              status: status,
//                              orderTime: orderTime,
//                              order: orderDetails,
//                              orderNum: orderNum,
//                              contactNumber: contactNumber,
//                              address: address,
//                              submitTime: now.getTime()
//                          };
//                          if (mobileNumber !== '') {
//                              orderObj['mobileNumber'] = mobileNumber;
//                          }
//                          db.collection("pendingOrders").doc().set(orderObj).then(function () {
//                              orderDetails = [];
//                              orderName = '';
//                              let orderDetailsResponse = {'orderName': orderName, 'orderNum': orderNum,};
//                              return response.send(orderDetailsResponse);
//                          }).catch(function (error) {
//                              console.error("Error writing document: ", error);
//                          });
//                      });
//                  });
//              } else {
//                  return response.send({'declined':true});
//              }
//
//             });
//         // rp(options).then(function (body) {
//         //
//         //     let xml = body
//         //     let MWResponseXML = JSON.parse(convert.xml2json(xml, {compact: true, spaces: 4}));
//         //     let orderNumDocRef = db.collection("orderNum").doc("sequentialNum");
//         //     let orderNum;
//         //     if (MWResponseXML.mwResponse.responseCode._text === '0') {
//         //         db.runTransaction(function (transaction) {
//         //             return transaction.get(orderNumDocRef).then(function (orderNumDoc) {
//         //                 if (!orderNumDoc.exists) {
//         //                     orderNumDocRef.set({number: 0});
//         //                 }
//         //                 orderNum = orderNumDoc.data().number + 1;
//         //                 if (orderNum > 999) {
//         //                     orderNum = 1;
//         //                 }
//         //                 transaction.update(orderNumDocRef, {number: orderNum});
//         //             }).then(function () {
//         //                 var now = new Date();
//         //                 var status = 'pending';
//         //                 if (orderTime == 'asap') {
//         //                     status = 'inProgress';
//         //                 }
//         //                 orderObj = {
//         //                     name: orderName,
//         //                     token: messageToken,
//         //                     status: status,
//         //                     orderTime: orderTime,
//         //                     order: orderDetails,
//         //                     orderNum: orderNum,
//         //                     contactNumber: contactNumber,
//         //                     address: address,
//         //                     submitTime: now.getTime()
//         //                 };
//         //                 if (mobileNumber !== '') {
//         //                     orderObj['mobileNumber'] = mobileNumber;
//         //                 }
//         //                 db.collection("pendingOrders").doc().set(orderObj).then(function () {
//         //                     orderDetails = [];
//         //                     orderName = '';
//         //                     let orderDetailsResponse = {'orderName': orderName, 'orderNum': orderNum,};
//         //                     return response.send(orderDetailsResponse);
//         //                 }).catch(function (error) {
//         //                     console.error("Error writing document: ", error);
//         //                 });
//         //             });
//         //         });
//         //     } else {
//         //         return response.send({'declined':true});
//         //     }
//         // }).catch(function (err) {
//         //     console.log('error in makepayment ' + err);
//         //     return response.send(err);
//         // });
//     }
//
//
//     function fuckMe(menuData) {
//         menuData.forEach(function (menuItem) {
//             for (let property in menuItem.extras) {
//
//                 if (menuItem.extras.hasOwnProperty(property) && Array.isArray(menuItem.extras[property])) {
//                     menuItem.extras[property].forEach(function (item) {
//                         priceMapping[item.id] = item.price;
//                     })
//                 }
//             }
//             if (menuItem.hasOwnProperty('perPersonPrice')) {
//                 menuItem.types.forEach(function (item) {
//                     banquetPriceMapping[item.id] = {
//                         'extraPrice': item.extraPrice,
//                         'price': item.price,
//                         'minPeople': menuItem.minPeople
//                     };
//                 });
//             } else {
//
//                 menuItem.types.forEach(function (item) {
//                     priceMapping[item.id] = item.price;
//
//                 });
//             }
//         });
//     }
//
//
//     function getTotalPrice(t) {
//         orderIds.forEach(function (key) {
//             if (priceMapping[key] != null) {
//                 t = t + priceMapping[key];
//             }
//             if (banquetPriceMapping[key] != null) {
//                 t = t + banquetPriceMapping[key].price;
//                 t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] ));
//                 // t = t + banquetPriceMapping[key].extraPrice * (parseInt(banquetPeopleMapping[key] - parseInt(banquetPriceMapping[key].minPeople)));
//             }
//
//
//         })
//         // Pin is in cents
//         t = t*100;
//         return Number(Math.round(t + 'e' + 2) + 'e-' + 2);
//     }
//
//     /***payment details is gonna be an array(check this here) if so check how to get object properties***/
//
//
//     function getResult(param) {
//         return crypto.createHash('md5').update(param).digest("hex");
//     }
//
//
// })



/*exports.addMobile = functions.https.onRequest((request, response) => {
  db.collection('project').get().then(function(snapshot) {
    snapshot.forEach(function (item) {
      thisProj = item.data();
    });
  }).then(function () {

    response.header('Access-Control-Allow-Origin', thisProj.allowOrigin);
        
    if (request.get('origin') != thisProj.allowOrigin) {
      return;
    }
    
    let mobileNumber = Buffer.from(request.body.mobileNumber, 'base64').toString();  
    let orderNumber = parseInt(request.body.orderNumber);  
    
    db.collection("pendingOrders").where("orderNum", "==", orderNumber).get().then(function(querySnapshot) {
      querySnapshot.forEach(function(doc) {
         doc.ref.update({"mobileNumber": mobileNumber});
        return response.send({'success': true});
      });
    }).catch(function(error) {
      return response.send({'success': false});
    });
  });
});*/


exports.completeOrder = functions.firestore.document('pendingOrders/{docId}').onUpdate((change,context)  => {
    const orderData = change.after.data();
    var oldData = change.before.data();
    // var thisProj;
    // db.collection('project').get().then(function(snapshot) {
    //     snapshot.forEach(function (item) {
    //         thisProj = item.data();
    //     });
    // })


    if (orderData.status == 'ready' && oldData.status == 'inProgress') {

      if(orderData.token == 'undefined' && orderData.mobileNumber){


        //do twilio stuff ...
        var details = {};
      
        db.collection('vendor').doc('details').get().then(function(doc) {
          details = doc.data();
        }).then(function () {

          var phoneNumber=orderData.mobileNumber;
          var customerName=orderData.name;
          let textMessage;
          if(isEmptyObject(orderData.address)  ){
              textMessage = {
                  body: 'Hey ' + customerName + ' order ' + orderData.orderNum + ' is ready for collection!',
                  to: phoneNumber,
                  // from: thisProj.smsShortTitle,
                  from:'belcobabs',
              };
          }else{
              textMessage = {
                  body: 'Hey ' + customerName + ' order ' + orderData.orderNum + ' is on its way for delivery!',
                  to: phoneNumber,
                  // from: thisProj.smsShortTitle,
                  from:'belcobabs',
              };
          }


          return client.messages.create(textMessage);
        });    
      }
      
      if (orderData.token != 'undefined') {

        var registrationToken = orderData.token;

        let message;
        if(isEmptyObject(orderData.address)  ){
            message = 'is ready for collection!';
        }else{
            message = 'is on it way for delivery!!';
        }

        const payload = {
          token: registrationToken,
          "notification": {
              "title": "Halal Pide House",
              "body": "Order "+ orderData.orderNum + "  " + message,
          },
          "data": {
              "status": "Hey "+ orderData.name + ", Order " + orderData.orderNum + "  " + message,
              "title": "Halal Pide House"
          },
        };

        return admin.messaging().send(payload);
      }
      return true;
    }
    
    return true;
});

isEmptyObject = function(obj) {
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
};


// Bender started here
exports.updateOrderStatus = functions.pubsub.schedule('* * * * *').onRun((context) => {
  
  db.collection('vendor').doc('details').get().then(function(vendorSnap) {
    var details = vendorSnap.data();    
    db.collection('pendingOrders').where("status", "==", "pending").onSnapshot(function (querySnapshot) {
      querySnapshot.forEach(function(docSnapshot) {
        var now = new Date();
        var thisOrder = docSnapshot.data();
        var orderTime = new Date(); 
        if (details.hasOwnProperty('orderEarlyMark')) {
          orderTime = new Date(parseInt(thisOrder.orderTime-(details.orderEarlyMark * 60 * 1000)));
        } else {
          orderTime = new Date(parseInt(thisOrder.orderTime));
        }
        if (orderTime.getTime() < now.getTime()) {
          thisOrder.submitTime = now.getTime();
          thisOrder.status = 'inProgress';
          db.collection('pendingOrders').doc(docSnapshot.id).update(thisOrder);
        }
      });
    });
  });
  return true;
});

exports.newOrderNotification1 = functions.firestore.document('pendingOrders/{docId}').onUpdate((change,context)  => {
  const orderData = change.after.data();
  const previousData = change.before.data();
  console.log(orderData);
  console.log(previousData);
  if (orderData.status == 'inProgress' && previousData.status == 'pending') {
      
      var details = {};
      
      db.collection('vendor').doc('details').get().then(function(doc) {
        details = doc.data();
      }).then(function () {

        if (details.hasOwnProperty('managerMobile')) {
          var textMessage = {
              body: 'A new online order has arrived and needs attention',
              to: details['managerMobile'],
              from: 'yarrababs',
          };
          client.messages.create(textMessage);
        }    
    
        db.collection('instore').get().then(function(snapshot) {
          snapshot.forEach(function (login) {
            const payload = {
              token: login.data().token,
              "notification": {
                "title": "Halal Pide House",
                "body": "A new order has arrived"
              },
              "data": {
                "status": "A new order has entered the In Progress queue and needs attention",
                "title": "Halal Pide House"
              }
            };
            admin.messaging().send(payload);
          });
        });
      });
    }
  return true;
});

exports.newOrderNotification2 = functions.firestore.document('pendingOrders/{docId}').onCreate((snap,context)  => {
  const orderData = snap.data();
  if (orderData.status == 'inProgress') {

     var details = {};
      
      db.collection('vendor').doc('details').get().then(function(doc) {
        details = doc.data();
      }).then(function () {

      if (details.hasOwnProperty('managerMobile')) {
        var textMessage = {
            body: 'A new online order has arrived and needs attention',
            to: details['managerMobile'],
            from: 'yarrababs',
        };
         client.messages.create(textMessage);
      }    
  
      db.collection('instore').get().then(function(snapshot) {
        snapshot.forEach(function (login) {
          const payload = {
            token: login.data().token,
            "notification": {
              "title": "Halal Pide House",
              "body": "A new order has arrived"
            },
            "data": {
              "status": "A new order has entered the In Progress queue and needs attention",
              "title": "Halal Pide House"
            }
          };
          admin.messaging().send(payload);
        });
      });
    });
  }
  return true;
});

