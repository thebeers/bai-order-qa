var ePosDev = new epson.ePOSDevice();
var printer = null;
var port = 8008;
var printerConnected = false;

function connect(printerIP) {
  ePosDev.connect(printerIP, port, cbConnect);
  /*ePosDev.onreconnecting = OnReconnecting;
  ePosDev.onreconnect = OnReconnect;
  ePosDev.ondisconnect = OnDisconnect;*/
}

function cbConnect(data) {
	if(data == 'OK') {
		ePosDev.createDevice('local_printer', ePosDev.DEVICE_TYPE_PRINTER, {'crypto' : true, 'buffer' : false}, cbCreateDevice_printer);
	} else {
		alert(data);
	}
}

function cbCreateDevice_printer(devobj, retcode) {
	if( retcode == 'OK' ) {
		printer = devobj;
    printerConnected = true;
	} else {
		alert(retcode);
	}
}

function printText(text) {
  printer.addLayout(printer.LAYOUT_RECEIPT, 290, 284, 0, 0, 0, 0);
  printer.addText(text);
  printer.addCut(printer.CUT_FEED);
  printer.send();
}