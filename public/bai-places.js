var Map_Action = {
    handleEvent: function(e) {
      fn = window[this.action];    
      if (typeof fn === 'function') {
        if (this.args !== '' && this.args != null) {
          fn(this.args);
        } else {
          fn();
        }
      }
    },
    action: '',
    args: ''
};

var address = {};
var deliveryRules;
var amount = 0;
var validAddress = false;

function initAutocomplete(deliveryFromCloud) {
   
  // Get info from cloud 
  deliveryRules = deliveryFromCloud;
  
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('autocomplete'), {types: ['geocode']});

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    autocomplete.setFields(['address_component']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}
    
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  var address_componentsMapping = {'street_number': 'street_number', 'route': 'street', 'locality': 'suburb', 'administrative_area_level_1': 'state', 'country': 'country', 'postal_code': 'postcode'};  
  
  // Get each component of the address from the place details,
  // and then fill-in the corresponding field in the address obj.
  validAddress = true;
  address = {};
  Object.keys(address_componentsMapping).forEach(function(requiredPart) {
    foundPart = false;
    for (var i = 0; i < place.address_components.length; i++) {
      if (place.address_components[i].types.includes(requiredPart)) {
        address[address_componentsMapping[requiredPart]] =  place.address_components[i].short_name;
        foundPart = true;
      }
    }
    if (!foundPart) {
      validAddress = false;
    }
  });
  /*for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (address_componentsMapping.hasOwnProperty(addressType)) {
      address[address_componentsMapping[addressType]] =  place.address_components[i].short_name;
    }
  }*/
  
  drawSelectedAddress();
}

function drawSelectedAddress() {
  addressDiv = document.getElementById('completedAddress');
  addressDiv.innerHTML = '';
  addressDiv.setAttribute('style', 'display: block; background-color: #e4e7ed; width=80%;');
  document.getElementById('autocomplete').value = '';
  
  if (validAddress) {
    
    if (address['country'] !== deliveryRules.country || !deliveryRules.states.includes(address['state']) || !deliveryRules.deliveryRange.includes(address['suburb'])) {
      addressDiv.appendChild(document.createTextNode('Delivery not available to selected suburb.'));
      validAddress = false;
      removeDeliveryCharges();

    } else {
  
      document.getElementById('autocomplete').setAttribute('style', 'display: none;');
      addressDiv.appendChild(drawNavBar([{text: 'Delivery Address'}],[{text: 'Edit', style: 'text-decoration: underline; font-size: x-small; cursor: pointer;', action: 'resetAddress'}]));
      addressDiv.appendChild(drawNavBar([{text: address['street_number'] + ' ' + address['street']}],null));
      addressDiv.appendChild(drawNavBar([{text: address['suburb'] + ' ' + address['state'] + ' ' + address['postcode']}],null));
      drawDeliveryCharges();
      enablePaymentBtn();
    }
  } else {
    addressDiv.appendChild(document.createTextNode('Delivery not available to selected location. Addresses require a valid street number.'));
    validAddress = false;
    removeDeliveryCharges();
  }
}

function getSerialisedAddress() {
  deliveryInst = document.getElementById('deliveryInstructions').value;
  if (deliveryInst !== '') {
    address['deliveryInst'] = deliveryInst;
  }
  return btoa(JSON.stringify(address));
}

function resetAddress() {
  document.getElementById('autocomplete').setAttribute('style', 'display: block; width: 80%;');
  document.getElementById('completedAddress').setAttribute('style', 'display: none;');
  document.getElementById('completedAddress').innerHtml = '';
  validAddress = false;
  enablePaymentBtn();
}

function isAddressValid() {
  return validAddress;
}
  
function drawDeliveryCharges() {
  
  // There's a long explanation on the interwebz but it turns out anonmyous functions are somehow added 
  // to an event listener twice (i think once on interpretting the javascript and another when rendering the 
  // associated element???). What then happens is this function is called twice and we double the delivery 
  // cost. Simple fix is to let it happen and have the function only do something if necessary. So if delivery 
  // charge already exists in the DOM, just replace it.
  
  if (document.getElementById('deliveryCharge') != 'undefined' && document.getElementById('deliveryCharge') != null) {
      removeDeliveryCharges();
  }
  
  document.getElementById('deliveryInstructions').setAttribute('style','display: block; width: 80%;');
  deliveryItem = {};
  currentTotal = parseFloat(document.getElementById('orderTotal').textContent);
  /*if (deliveryRules.discountSuburbs.includes(address['suburb'])) {
    deliveryItem = {menuCat: 'Delivery fee', type: [{name: 'Discount rate'}], itemTotal: deliveryRules.discountPrice};
    amount = deliveryRules.discountPrice;
  } else {
    deliveryItem = {menuCat: 'Delivery fee', type: [{name: 'Standard rate'}], itemTotal: deliveryRules.fullPrice};
    amount = deliveryRules.fullPrice;
  }*/
  if (deliveryRules.deliveryRange.includes(address['suburb']) && currentTotal > deliveryRules.minOrderPrice) {
    /*if (currentTotal > deliveryRules.minOrderPrice) {
      deliveryItem = {menuCat: 'Delivery fee', type: [{name: 'Discount rate'}], itemTotal: deliveryRules.discountPrice};
      amount = deliveryRules.discountPrice;
    } else {*/
      deliveryItem = {menuCat: 'Delivery fee', type: [{name: 'Standard rate'}], itemTotal: deliveryRules.fullPrice};
      amount = deliveryRules.fullPrice;
    //}
    deliveryCharge = drawCheckoutCard(deliveryItem);
    deliveryCharge.setAttribute('id','deliveryCharge');
    document.getElementById('orderDetails').appendChild(deliveryCharge);
  }
    
  newTotal = currentTotal + amount;
  document.getElementById('orderTotal').innerHTML = newTotal.toString();
  
}

function removeDeliveryCharges() {
  deliveryCharge = document.getElementById('deliveryCharge');
  deliveryCharge.parentNode.removeChild(deliveryCharge);
  currentTotal = parseFloat(document.getElementById('orderTotal').textContent);
  newTotal = currentTotal - amount;
  document.getElementById('orderTotal').innerHTML = newTotal.toString();
}

function getDeliveryAmount() {
  return amount;
}


// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

