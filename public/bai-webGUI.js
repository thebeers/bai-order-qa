

var buttonMap = {add: 'fa-plus', cart: 'fa-shopping-basket', back: 'fa-arrow-left', thumb: 'fa-thumbs-up', printBtn: 'fa_print', forward: 'fa-arrow-right'};

var BAI_Action = {
    handleEvent: function(e) {
      if (e.type == 'click') {
        fn = window[this.action];    
        if (typeof fn === 'function') {
          if (this.args !== '' && this.args != null) {
            fn(this.args);
          } else {
            fn();
          }
        }
      }
    },
    action: '',
    args: ''
};

function drawNavBar (left, right) {
    para = document.createElement('p');
    para.setAttribute('style', 'text-align:left;'); 
    if (left != null) {
      leftSpan = buildSpan(left, 'left');
      para.appendChild(leftSpan);  
    }   
    if (right != null) {
      rightSpan = buildSpan(right, 'right');
      rightSpan.setAttribute('style', 'float:right;');
      para.appendChild(rightSpan);  
    }  
    return para;
}

function drawTimeSelector(id, tradingHours) {
  ddDiv = document.createElement('div');
  ddLabel = document.createElement('label');
  ddLabel.setAttribute('for',id);
  ppText = document.createTextNode('Place order at time:');
  ddLabel.appendChild(ppText);
  ddDiv.appendChild(ddLabel);
  ddList = document.createElement('select');
  ddList.setAttribute('id',id);
  ddList.setAttribute('style','margin-left: 5px;');
  /*if (!beforeOpeningTime(new Date(), tradingHours)) {
    listOpt = document.createElement('option');
    listOpt.appendChild(document.createTextNode('Now'));
    listOpt.setAttribute('value','asap');
    ddList.appendChild(listOpt);
  }*/
  ddList.innerHTML = updateTimeSelector(id, tradingHours, false);
  ddDiv.appendChild(ddList);
  return ddDiv;
}

function updateTimeSelector (listId, hourRange, liveUpdate) {
  listString = '';
  displayTime = getNearestReasonableTime(hourRange);
  count = 0;
  while (!pastClosingTime(displayTime, hourRange)) {
    listOpt = document.createElement('option');
    listOpt.setAttribute('value',displayTime.getTime());
    hours = displayTime.getHours();
    period = 'AM'
    if (hours >= 12) {
      period = 'PM'; 
    }
    if (hours > 12) {
      hours-=12;
    }
    optText = document.createTextNode(zeroPad(hours) + ':' + zeroPad(displayTime.getMinutes()) + ' ' + period);
    listOpt.appendChild(optText);
    listString += listOpt.outerHTML;
    displayTime.setTime(displayTime.getTime()+(15*60*1000));
    count++;
    if (count > 50) {
        break;
    }
  }
  if (liveUpdate) {
    document.getElementById(listId).innerHTML = listString;
  } else {
    return listString;
  } 
}

function buildSpan(buildList, side) {
  newSpan = document.createElement('span');
  buildList.forEach(function(buildSpec) {
    var buildItem;
    if (buildSpec.hasOwnProperty('button')) {
      buildItem = document.createElement('button');
      buildItem.setAttribute('class', 'btn btn-default btn-sm');
      iconElement = document.createElement('i');
      iconElement.setAttribute('class', 'fas ' + buttonMap[buildSpec.button]);
      if (!buildSpec.hasOwnProperty('iconPos') || buildSpec.iconPos == 'right') {
        iconElement.setAttribute('style','margin-left: 6px;');
        buildItem.appendChild(document.createTextNode(buildSpec.label));  
        buildItem.appendChild(iconElement);
      } else {
        iconElement.setAttribute('style','margin-right: 6px;');
        buildItem.appendChild(iconElement)
        buildItem.appendChild(document.createTextNode(buildSpec.label));  
      }
      if (buildSpec.hasOwnProperty('id')) {
        buildItem.setAttribute('id', buildSpec.id);  
      }
    }
    if (buildSpec.hasOwnProperty('radio')) {
      buildItem = document.createElement('span');
      radioItem = document.createElement('input');
      radioItem.setAttribute('type','radio');
      radioItem.setAttribute('value', buildSpec.value);
      radioItem.setAttribute('name', buildSpec.name);
      if (buildSpec.hasOwnProperty('action')) {
        var myAction = Object.create(BAI_Action);
        myAction.action = buildSpec.action;
        myAction.args = buildSpec.args;
        radioItem.addEventListener('click', myAction);
      } 
      if (buildSpec.hasOwnProperty('checked') && buildSpec.checked === true) {
        radioItem.setAttribute('checked','true');
      }
      if (buildSpec.hasOwnProperty('id')) {
        radioItem.setAttribute('id', buildSpec.id);  
      }
      labelItem = document.createTextNode(buildSpec.radio);
      if (buildSpec.hasOwnProperty('label') && buildSpec.label === 'left') {
          buildItem.appendChild(labelItem);
          radioItem.setAttribute('style','margin-left: 6px');
          buildItem.appendChild(radioItem);
      } else {
          radioItem.setAttribute('style','margin-right: 6px');
          buildItem.appendChild(radioItem);
          buildItem.appendChild(labelItem);
      }
    }
    if (buildSpec.hasOwnProperty('checkbox')) {
      buildItem = document.createElement('span');
      checkItem = document.createElement('input');
      checkItem.setAttribute('type','checkbox');
      checkItem.setAttribute('value', buildSpec.value);
      checkItem.setAttribute('name', buildSpec.name);
      if (buildSpec.hasOwnProperty('action')) {
        var myAction = Object.create(BAI_Action);
        myAction.action = buildSpec.action;
        myAction.args = buildSpec.args;
        checkItem.addEventListener('click', myAction);
      } 
      if (buildSpec.hasOwnProperty('checked') && buildSpec.checked === true) {
        checkItem.setAttribute('checked','true');
      }
      if (buildSpec.hasOwnProperty('id')) {
        checkItem.setAttribute('id', buildSpec.id);  
      }
      labelItem = document.createTextNode(buildSpec.checkbox);
      if (buildSpec.hasOwnProperty('label')) {
        if (buildSpec.label === 'left') {
          buildItem.appendChild(labelItem);
          checkItem.setAttribute('style','margin-left: 6px');
          buildItem.appendChild(checkItem);
        } else if (buildSpec.label === 'right') {
          checkItem.setAttribute('style','margin-right: 6px');
          buildItem.appendChild(checkItem);
          buildItem.appendChild(labelItem);
        } else {
          buildItem.appendChild(checkItem);
        }
      }
    }
    if (buildSpec.hasOwnProperty('text')) {
      buildItem = document.createElement('span');
      buildItem.appendChild(document.createTextNode(buildSpec.text));
      if (buildSpec.hasOwnProperty('id')) {
        buildItem.setAttribute('id', buildSpec.id);  
      }
    }
    if (buildSpec.hasOwnProperty('textInput')) {
      buildItem = document.createElement('input');
      buildItem.setAttribute('type','text');
      if (buildSpec.hasOwnProperty('label') && buildSpec.label == 'inline') {
        buildItem.setAttribute('placeholder',buildSpec.textInput);
      } else if (buildSpec.label != null) {
        buildLabel = document.createElement('label');
        buildLabel.setAttribute('for',buildSpec.id);
        buildLabelTxt = document.createTextNode(buildSpec.textInput);
        buildLabel.appendChild(buildLabelTxt);
        buildLabel.setAttribute('style','margin-right: 3px');
        newSpan.appendChild(buildLabel);
      }
      if (buildSpec.hasOwnProperty('id')) {
        buildItem.setAttribute('id', buildSpec.id);  
      }
      if (buildSpec.hasOwnProperty('name')) {
        buildItem.setAttribute('name',buildSpec.name);
      }
    }
    if (buildSpec.hasOwnProperty('heading')) {
      buildItem = document.createElement('span');
      buildItem.setAttribute('style', 'font-size: 22px; font-weight: 300; color: rgba(0,0,0,0.6); margin: 10px 0 0;');
      buildItem.appendChild(document.createTextNode(buildSpec.heading));
      if (buildSpec.hasOwnProperty('id')) {
        buildItem.setAttribute('id', buildSpec.id);  
      }
    }
    if (buildSpec.hasOwnProperty('div')) {
      buildItem = document.createElement('div');
      buildItem.setAttribute('id', buildSpec['div']);
      if (buildSpec.hasOwnProperty('class')) {
        buildItem.setAttribute('class', buildSpec['class']);
      }
    }
    if (buildSpec.hasOwnProperty('style')) {
      buildItem.setAttribute('style', buildSpec.style);  
    }
    if (buildSpec.hasOwnProperty('action')) {
      var myAction = Object.create(BAI_Action);
      myAction.action = buildSpec.action;
      myAction.args = buildSpec.args;
      buildItem.addEventListener('click', myAction);
    }   
    existingStyle = buildItem.getAttribute('style');
    if (side == 'left') {
      buildItem.setAttribute('style','margin-left: 3px;' + existingStyle);
    }
    if (side == 'right') {
      buildItem.setAttribute('style','margin-right: 3px;' + existingStyle);  
    }

    newSpan.appendChild(buildItem);
  });
  return newSpan;
}

function drawInputwCols(label, inputElement, widths) {
  inputRow = document.createElement('div');
  inputRow.setAttribute('class','row');
  labelCol = document.createElement('div');
  labelCol.setAttribute('class',widths[0]);
  labelCol.appendChild(document.createTextNode(label));
  inputRow.appendChild(labelCol);
  inputCol = document.createElement('div');
  inputCol.setAttribute('class',widths[1]);
  inputElement.setAttribute('style','width: 80%;');
  inputCol.appendChild(inputElement);
  inputRow.appendChild(inputCol);
  return inputRow;
}

function drawButton(buttonSpecs, side) {
   button = document.createElement("button");
   button.setAttribute('id', buttonSpecs.id);
   button.setAttribute('class', 'btn btn-default btn-sm');   
   if (side == 'left') {
     button.setAttribute('style','margin-right: 3px;');
   } else {
     button.setAttribute('style','margin-left: 3px;');  
   }
   iconElement = document.createElement('i');
   iconElement.setAttribute('class', 'fas ' + buttonMap[buttonSpecs.button]);
   if (!buttonSpecs.hasOwnProperty('iconPos') || buttonSpecs.iconPos == 'right') {
     iconElement.setAttribute('style','margin-left: 6px;');
     button.appendChild(document.createTextNode(buttonSpecs.label));  
     button.appendChild(iconElement);
   } else {
     iconElement.setAttribute('style','margin-right: 6px;');
     button.appendChild(iconElement)
     button.appendChild(document.createTextNode(buttonSpecs.label));  
   }
   var myAction = Object.create(BAI_Action);
   myAction.action = buttonSpecs.action;
   myAction.args = buttonSpecs.args;
   button.addEventListener('click', myAction);
   return button;
}

function drawMenuCard() {
    
}

function drawCheckoutCard(orderItem) {
  card = document.createElement("div");
  card.className = "card";
  var cardHeader = document.createElement("div");
  cardHeader.className = "card-header";
  cardHeader.setAttribute('style','padding: 0px 15px;');
  rightSide = null;
  if (orderItem.hasOwnProperty('allowRemove') && orderItem['allowRemove'] === true) {
    rightSide = [{text: 'Remove item', style: 'text-decoration: underline; font-size: x-small; cursor: pointer;', action: 'removeItem', args: itemIndex}]
  }
  cardHdrSpan = drawNavBar([{text: orderItem.menuCat}], rightSide);
  cardHeader.appendChild(cardHdrSpan);
  card.appendChild(cardHeader);
  var cardBody = document.createElement("div");
  cardBody.className = "card-body";
  cardBody.setAttribute('style','padding-bottom: 0;');
  var addonList = document.createElement("ul");
  for (var key in orderItem) {
    if (Array.isArray(orderItem[key])) {
      orderItem[key].forEach(function (itemPart) {
        var li = document.createElement("li");
        var txt;
        if (itemPart.hasOwnProperty('qty')) {
            numberOfPeople = itemPart.qty;
            txt = document.createTextNode(itemPart.name + ' for ' + numberOfPeople + ' People');
          } else {  
            txt = document.createTextNode(itemPart.name);
          }
        li.appendChild(txt);
        addonList.appendChild(li);
      });
    }
  }

  cardBody.appendChild(addonList);

  commentInput = null;
  if (orderItem.hasOwnProperty('allowComment') && orderItem['allowComment'] === true) {
    commentInput = [{textInput: 'Item comment', label: 'inline', name: 'comment', id: 'comment_'+itemIndex}];
  }
  cardFooter = drawNavBar(commentInput,[{text: orderItem.itemTotal}]);
  cardBody.appendChild(cardFooter);
  card.appendChild(cardBody);
  return card;
}

function drawCard(cardContent) {
  var card = document.createElement("div");
  card.className = "card";
  card.setAttribute('id','card_'+cardContent.id);
  var cardHeader = document.createElement("div");
  cardHeader.className = "card-header";
  cardHeaderTxt = cardContent.name + ' - Order ' + cardContent.id;
  if (cardContent.hasOwnProperty('orderTime')) {
    var orderDate = new Date(parseInt(cardContent['orderTime']));
    hours = orderDate.getHours();
    period = 'AM'
    if (hours >= 12) {
      period = 'PM'; 
    }
    if (hours > 12) {
      hours-=12;
    }
    cardHeaderTxt += ' - ' + zeroPad(hours).toString() + ':' + zeroPad(orderDate.getMinutes()).toString() + ' ' + period;
  }
  var txt = document.createTextNode(cardHeaderTxt);
  cardHeader.appendChild(txt);
  card.appendChild(cardHeader);
  
  var cardBody = document.createElement("div");
  for (index = 0; index < cardContent.list.length; index++) {
    let menuCat = document.createTextNode(cardContent.list[index].menuCat);
    let numberOfPeople = 0;
   
    //cardBody.appendChild(menuCat);
    sectionP = document.createElement('p');
    sectionP.setAttribute('style','margin-bottom:2px; margin-top:22px;');
    var txt;
    if (cardContent.list[index]['type'][0].hasOwnProperty('qty')) {
      numberOfPeople = cardContent.list[index]['type'][0].qty;
      txt = cardContent.list[index]['type'][0].name + ' for ' + numberOfPeople + ' People';
    } else {  
      txt = cardContent.list[index]['type'][0].name;
    }
    sectionP.appendChild(document.createTextNode(cardContent.list[index].menuCat + ' - ' + txt));
    if (cardContent.list[index].singlePrint) {
      printItemBtn = drawButton({button: 'print', id: cardContent.id + '-' + index, action: 'printSingle', args: cardContent.id + '-' + index, label: 'Print Item'}, 'right');
      sectionP.appendChild(printItemBtn);
    }
    cardBody.appendChild(sectionP);
    for (var key in cardContent.list[index]) {   
      if (key != 'type' && Array.isArray(cardContent.list[index][key])) {
        if (key != 'base_radio') {
          printKey = key.replace('_radio','');
          sectionP = document.createElement('p');
          sectionP.setAttribute('style','margin-bottom:2px; margin-top:2px;');
          sectionP.appendChild(document.createTextNode(printKey));
          cardBody.appendChild(sectionP);
        }
        var addonList = document.createElement("ul");
        addonList.setAttribute('style','margin-bottom:2px;');
        cardContent.list[index][key].forEach(function (itemPart) {
          var li = document.createElement("li");
          li.appendChild(document.createTextNode(itemPart.name));
          addonList.appendChild(li);
        });
        cardBody.appendChild(addonList);
      }
    }
    if (cardContent.list[index].hasOwnProperty('comment') && cardContent.list[index]['comment'] != '') {
      sectionP = document.createElement('p');
      sectionP.setAttribute('style','margin-bottom:2px; margin-top:2px;');
      sectionP.appendChild(document.createTextNode('comments'));
      cardBody.appendChild(sectionP);
      commentsUl = document.createElement('ul');
      commentsUl.setAttribute('style','margin-bottom:2px;');
      commentsLi = document.createElement('li');
      commentsTxt = document.createTextNode(cardContent.list[index].comment);
      commentsLi.appendChild(commentsTxt);
      commentsUl.appendChild(commentsLi);
      cardBody.appendChild(commentsUl);
    }

  }
  if (cardContent.hasOwnProperty('htmlMessage')) {
    cardBody.appendChild(cardContent.htmlMessage);
  }
  if (cardContent.hasOwnProperty('footer')) {
    ftrSpan = drawNavBar(cardContent.footer.left,cardContent.footer.right);
    cardBody.appendChild(ftrSpan);
  }
  card.appendChild(cardBody);
  return card;
}

function formatAddress (address, header) {
  addressDiv = document.createElement('div');
  addressDiv.appendChild(document.createTextNode(header));
  addressDiv.appendChild(document.createElement('br'));
  if (address.hasOwnProperty('deliveryInst')) {
    addressDiv.appendChild(document.createTextNode(address.deliveryInst));
    addressDiv.appendChild(document.createElement('br'));
  }
  addressDiv.appendChild(document.createTextNode(address.street_number + ' ' + address.street));
  addressDiv.appendChild(document.createElement('br'));
  addressDiv.appendChild(document.createTextNode(address.suburb + ' ' + address.state + ' ' + address.postcode));
  addressDiv.appendChild(document.createElement('br'));
  return addressDiv;
}

function getLoginForm() {

  form = document.createElement('div');
  userRow = document.createElement('div');
  userRow.setAttribute('class', 'row');
  uLabelCol = document.createElement('div');
  uLabelCol.setAttribute('class', 'col-sm-2');
  uLabel = document.createElement('label');
  uLabel.setAttribute('for', 'user');
  uTxt = document.createTextNode('Username');
  uLabel.appendChild(uTxt);
  uLabelCol.appendChild(uLabel);
  userRow.appendChild(uLabelCol);
  uInputCol = document.createElement('div');
  uInputCol.setAttribute('class', 'col');
  uInput = document.createElement('input');
  uInput.setAttribute('type', 'text');
  uInput.setAttribute('id', 'user');
  uInput.setAttribute('value', '');
  uInput.setAttribute('required', 'true');
  uInputCol.appendChild(uInput);
  userRow.appendChild(uInputCol);
  form.appendChild(userRow);
  passRow = document.createElement('div');
  passRow.setAttribute('class', 'row');
  pwLabelCol = document.createElement('div');
  pwLabelCol.setAttribute('class', 'col-sm-2');
  pwLabel = document.createElement('label');
  pwLabel.setAttribute('for', 'pw');
  pwTxt = document.createTextNode('Password');
  pwLabel.appendChild(pwTxt);
  pwLabelCol.appendChild(pwLabel);
  passRow.appendChild(pwLabelCol);
  pwInputCol = document.createElement('div');
  pwInputCol.setAttribute('class', 'col');
  pwInput = document.createElement('input');
  pwInput.setAttribute('type', 'password');
  pwInput.setAttribute('id', 'pw');
  pwInput.setAttribute('placeholder', 'Enter Password');
  pwInput.setAttribute('required', 'true');
  pwInput.addEventListener('keyup', function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      login();
    }
  });
  pwInputCol.appendChild(pwInput);
  passRow.appendChild(pwInputCol);
  form.appendChild(passRow);
  loginBtn = document.createElement('button');
  loginBtn.setAttribute('type', 'submit');
  loginBtn.addEventListener('click', login);
  loginTxt = document.createTextNode('Login');
  loginBtn.appendChild(loginTxt);
  form.appendChild(loginBtn);
  return form;
}

function login() {
  email = document.getElementById('user').value;
  password = document.getElementById('pw').value;
  firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log(errorCode);
    console.log(errorMessage);
  });
}

function getNearestReasonableTime(tradingHours) {
  now = new Date();
  tradingHours.open[now.getDay()];
  openParts = tradingHours.open[now.getDay()].split(':');
  openTimeStr = now.getFullYear() + '/' + (now.getMonth()+1) + '/' + now.getDate() + ' ' + openParts[0] + ':' + openParts[1] + ':00 ' + tradingHours.timezone;
  openTime = new Date(openTimeStr);
  if (now.getTime() > openTime.getTime()) {
    return new Date(Math.floor(now.getTime()/1000/60/15+1)*15*60*1000);
  } else {
    return openTime;
  }
}

function pastClosingTime(suspect, tradingHours) {
  now = new Date();
  closingParts = tradingHours.close[now.getDay()].split(':');
  closingTimeStr = now.getFullYear() + '/' + (now.getMonth()+1) + '/' + now.getDate() + ' ' + closingParts[0] + ':' + closingParts[1] + ':00 ' + tradingHours.timezone;
  closingTime = new Date(closingTimeStr);
  if (suspect.getTime() > closingTime.getTime()) {
    return true;
  } else {
    return false;
  }
}

function beforeOpeningTime(suspect, tradingHours) {
  now = new Date();
  openParts = tradingHours.open[now.getDay()].split(':');
  openTimeStr = now.getFullYear() + '/' + (now.getMonth()+1) + '/' + now.getDate() + ' ' + openParts[0] + ':' + openParts[1] + ':00 ' + tradingHours.timezone;
  openTime = new Date(openTimeStr);
  if (now.getTime() < openTime.getTime()) {
    return true;
  } else {
    return false;
  }
}

function getLocalTime(epochSeconds) {
  dateObj = new Date(epochSeconds);
  hours = dateObj.getHours();
  period = 'AM'
  if (hours >= 12) {
    period = 'PM'; 
  }
  if (hours > 12) {
    hours-=12;
  }
  return zeroPad(hours).toString() + ':' + zeroPad(dateObj.getMinutes()).toString() + ' ' + period;
}

function zeroPad(i) {
  var numStr = '';
  if (i < 10) {
    numStr = '0';
  }
  numStr += i.toString();
  return numStr;
}